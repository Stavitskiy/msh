<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Checklist_item extends Model
{
    //use SoftDeletes;

    //protected $dates = ['deleted_at'];
    protected $fillable = ['checklist_id', 'value',];

    public static function saveData(Request $request)
    {
        $checklist = Checklist::checklist($request);

        if (isset($checklist)) {
            return Checklist_item::create([
                'checklist_id' => $checklist->id,
                'value' => $request->value,
            ]);
        }
    }

    public static function softDelete(Request $request)
    {
        $checklist_item = Checklist_item::find($request->checklist_item_id);

        if (isset($checklist_item)) {
            if ($checklist_item->delete()) {
                return true;
            }
        }

        return false;
    }
}

