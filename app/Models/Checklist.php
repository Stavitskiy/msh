<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;

class Checklist extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = ['user_id', 'title',];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function checklistItem()
    {
        return $this->hasMany('App\Models\Checklist_item');
    }

    public static function checklist(Request $request)
    {
        return Checklist::where('id', '=', $request->id)
            ->where('user_id', '=', $request->user()->id)
            ->with('checklistItem')
            ->first();
    }

    public static function saveData(Request $request)
    {
        return Checklist::create([
            'user_id' => $request->user()->id,
            'title' => $request->title
        ]);
    }

    public static function softDelete(Request $request)
    {
        $checklist = self::checklist($request);

        if (isset($checklist)) {
            if($checklist->delete()){
                return true;
            };
        }

        return false;
    }
}
