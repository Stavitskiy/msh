<?php

namespace App\Http\Middleware;

use Closure;

class CheckIssetAuthToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(empty($request->header('Authorization'))){
            return response()->json('Missing Auth Token ( Bearer )');
        }

        return $next($request);
    }
}
