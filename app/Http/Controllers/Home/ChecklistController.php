<?php
/**
 * Created by PhpStorm.
 * User: St
 * Date: 03.12.2018
 * Time: 17:22
 */

namespace App\Http\Controllers\Home;


use App\Http\Controllers\Controller;
use App\Models\Checklist;

class ChecklistController extends Controller
{
    public function index()
    {
        $checklists = Checklist::with('user','checklistItem')->get();

        return view('checklist.index',compact('checklists'));
    }
}