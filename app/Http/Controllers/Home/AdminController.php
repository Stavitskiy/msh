<?php
/**
 * Created by PhpStorm.
 * User: St
 * Date: 03.12.2018
 * Time: 12:16
 */

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAddModerator;
use App\Models\RoleUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use jeremykenedy\LaravelRoles\Models\Role;

class AdminController extends Controller
{
    public function index()
    {
        $users = User::all();

        foreach ($users as $user){
            if ($user->hasRole(['senior.moderator','middle.moderator','junior.moderator'])) {
                $moderators[] = $user;
            }
        }

        return view('administration.index', compact('moderators'));
    }

    public function create()
    {
        return view('administration.form');
    }

    public function store(StoreAddModerator $request)
    {
        $request->validated();
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        if (isset($user)) {
            $role = Role::where('name', '=', $request->role)->first();
            $user->attachRole($role);

            return redirect(route('administration'));
        }
    }

    public function delete(Request $request)
    {
        if (User::findOrFail($request->id)->delete()) {
            return redirect(route('administration'));
        }
    }
}