<?php
/**
 * Created by PhpStorm.
 * User: St
 * Date: 03.12.2018
 * Time: 12:17
 */

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function index()
    {
        return view('users.index');
    }
}