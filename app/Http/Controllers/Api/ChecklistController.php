<?php
/**
 * Created by PhpStorm.
 * User: St
 * Date: 02.12.2018
 * Time: 19:54
 */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Checklist;
use App\Models\Checklist_item;
use Illuminate\Http\Request;

class ChecklistController extends Controller
{
    // TODO вынести $request->user() в конструктор если получится!!! Middleware?

    public function getAll(Request $request)
    {
        $checklists = Checklist::where('user_id', '=', $request->user()->id)
            ->with('checklistItem')
            ->get();

        if (isset($checklists)) {
            return response()->json($checklists);
        } else {
            return response()->json('Checklists not find');
        }
    }

    public function get(Request $request)
    {
        $checklist = Checklist::checklist($request);
        if (isset($checklist)) {
            return response()->json($checklist);
        } else {
            return response()->json('Checklist not find');
        }
    }

    public function count(Request $request)
    {
        $data = [
            'Maximum amount: ' => $request->user()->count_checklist,
            'Already in use: ' => $request->user()->checklist->count(),
            'New available: ' => $request->user()->count_checklist - $request->user()->checklist->count()
        ];

        return response()->json($data);
    }

    public function add(Request $request)
    {
        if (($request->user()->count_checklist - $request->user()->checklist->count()) > 0) {
            $request->validate([
                'title' => 'bail|required|string|max:70',
            ]);

            $new_checklist = Checklist::saveData($request);
            if (isset($new_checklist)) {
                return response()->json($new_checklist,201);
            } else {
                return response()->json('Ups, error saving data');
            }
        } else {
            return response()->json('Reached the maximum number of checklists');
        }
    }

    public function delete(Request $request)
    {
        $request->validate([
            'id' => 'bail|required|integer',
        ]);

        $delete_checklist = Checklist::softDelete($request);

        if ($delete_checklist === true) {
            return response()->json('Checklist successfully deleted');
        } else {
            return response()->json('Checklist with specified id does not exist');
        }
    }

    public function addItemToChecklist(Request $request)
    {
        $request->validate([
            'id' => 'bail|required|integer',
            'value' => 'bail|required|string|max:70',
        ]);

        $new_checklist_item = Checklist_item::saveData($request);

        if (isset($new_checklist_item)) {
            return response()->json($new_checklist_item,201);
        } else {
            return response()->json('Ups, error saving data. The specified checklist may not exist.');
        }
    }

    public function deleteChecklistItem(Request $request)
    {
        $request->validate([
            'checklist_item_id' => 'bail|required|integer',
        ]);

        $delete_checklist_item = Checklist_item::softDelete($request);

        if ($delete_checklist_item === true) {
            return response()->json('Checklist item successfully deleted');
        } else {
            return response()->json('Checklist item with specified id does not exist');
        }
    }
}