<?php

use Illuminate\Database\Seeder;
use jeremykenedy\LaravelRoles\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Role::where('name', '=', 'Admin')->first() === null) {
            $adminRole = Role::create([
                'name' => 'Admin',
                'slug' => 'admin',
                'description' => 'Admin Role',
                'level' => 5,
            ]);
        }

        if (Role::where('name', '=', 'Senior-moderator')->first() === null) {
            $adminRole = Role::create([
                'name' => 'Senior Moderator',
                'slug' => 'senior-moderator',
                'description' => 'Senior Moderator Role',
                'level' => 4,
            ]);
        }

        if (Role::where('name', '=', 'Middle-moderator')->first() === null) {
            $adminRole = Role::create([
                'name' => 'Middle Moderator',
                'slug' => 'middle-moderator',
                'description' => 'Middle Moderator Role',
                'level' => 3,
            ]);
        }

        if (Role::where('name', '=', 'Junior-moderator')->first() === null) {
            $adminRole = Role::create([
                'name' => 'Junior Moderator',
                'slug' => 'Junior-moderator',
                'description' => 'Junior Moderator Role',
                'level' => 2,
            ]);
        }

        if (Role::where('name', '=', 'User')->first() === null) {
            $userRole = Role::create([
                'name' => 'User',
                'slug' => 'user',
                'description' => 'User Role',
                'level' => 1,
            ]);
        }
    }
}
