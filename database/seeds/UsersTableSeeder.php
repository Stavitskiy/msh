<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use jeremykenedy\LaravelRoles\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::where('name', '=', 'Admin')->first();

        $user = User::create([
            'name' => 'Admin Rootovich',
            'email' => 'admin@homework.com',
            'password' => Hash::make('password'),
        ]);

        $user->attachRole($adminRole);
    }
}
