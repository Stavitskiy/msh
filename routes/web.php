<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', function () {
    return redirect(route('login'));
});


Route::middleware(['auth', 'level:2'])
    ->prefix('home')->group(function () {
        Route::get('/', 'Home\HomeController@index')->name('home');
        Route::get('change-password', 'Home\HomeController@changePassword')->name('change password');
        // TODO post с формы смена пароля

        Route::prefix('administration')->group(function () {
            Route::get('/', 'Home\AdminController@index')->name('administration');
            Route::get('create', 'Home\AdminController@create')->name('adm_create');
            Route::post('create', 'Home\AdminController@store')->name('adm_store');
            Route::post('delete', 'Home\AdminController@delete')->name('adm_delete');
        });

        Route::prefix('checklists')->group(function () {
            Route::get('/', 'Home\ChecklistController@index');
        });

        Route::prefix('users')->group(function () {
            Route::get('/', 'Home\UserController@index');
            Route::get('/{id}', 'Home\UserController@detail')->name('usr_detail');
            Route::get('/update', 'Home\UserController@update')->name('usr_update');
        });
    });




