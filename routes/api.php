<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('signup', 'Api\AuthController@signup');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('user', 'Api\AuthController@user');
    });
});

Route::group(['middleware' => ['token','auth:api']], function () {
    Route::prefix('checklist')->group(function () {
        Route::get('', 'Api\ChecklistController@getAll');
        Route::get('/count ', 'Api\ChecklistController@count');
        Route::get('/{id}', 'Api\ChecklistController@get');
        Route::post('add', 'Api\ChecklistController@add');
        Route::post('delete', 'Api\ChecklistController@delete');
        Route::prefix('item')->group(function () {
            Route::post('add', 'Api\ChecklistController@addItemToChecklist');
            Route::post('delete', 'Api\ChecklistController@deleteChecklistItem');
        });
    });
});
