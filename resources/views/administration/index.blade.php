@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Index administration</h1>
@stop

@section('content')
    <a href="{{ url(route('adm_create')) }}" class="btn btn-info" role="button">Create Moderator</a>
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th>Date Created</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                @forelse($moderators as $moderator)
                    <tr>
                        <td>{{ $moderator->id }}</td>
                        <td>{{ $moderator->email }}</td>
                        <td>
                            @if($moderator->level() == 4)
                                Senior Moderator
                            @elseif ($moderator->level() == 3)
                                Middle Moderator
                            @elseif($moderator->level() == 2)
                                Junior Moderator
                            @endif
                        </td>
                        <td>{{ date_format($moderator->created_at,'d.m.Y')}}</td>
                        <td>
                            <form action="{{url(route('adm_delete'))}}" method="post">
                                {{ csrf_field() }}
                                <input type="submit" class="btn btn-info" value="Delete">
                                <input type="hidden" name="id" value="{{ $moderator->id }}">
                            </form>
                        </td>
                    </tr>
                @empty
                    <th> -</th>
                    <th> -</th>
                    <th> -</th>
                    <th> -</th>
                    <th> -</th>
                    <th> -</th>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@stop