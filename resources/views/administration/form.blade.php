@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')

    <h1>Add new Moderator</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-offset-3">
            <div class="col-md-6">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="post" action="{{ url(route('adm_store')) }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" id="password" name="password">
                    </div>
                    <div class="form-group">
                        <label for="password_confirmation">Confirm password:</label>
                        <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
                    </div>
                    <div class="form-group">
                        <label for="role">Role:</label>
                        <select class="form-control" id="role" name="role" >
                            <option value="{{ old('role') }}">{{ old('role') }}</option>
                            <option value="Senior Moderator">Senior Moderator</option>
                            <option value="Middle Moderator">Middle Moderator</option>
                            <option value="Junior Moderator">Junior Moderator</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-default">Add Moderator</button>
                </form>
            </div>
        </div>
    </div>
@stop