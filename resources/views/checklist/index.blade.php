@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Index Checklist</h1>
@stop

@section('content')
    @forelse($checklists as $checklist)
        <div class="col-md-6">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse"
                               href="#collapse{{$checklist->id}}">{{ $checklist->title }}</a>
                        </h4>
                    </div>
                    <div id="collapse{{$checklist->id}}" class="panel-collapse collapse">
                        <div class="panel-body">@forelse($checklist->checklistItem as $item)
                                <div>
                                    <p>{{ $item->value }}
                                        <span> - @if(is_null($item->status)) Active @else Done @endif</span>
                                    </p>
                                </div>
                            @empty
                            @endforelse</div>
                    </div>
                </div>
            </div>
        </div>
    @empty
        Not Checklists
    @endforelse
@stop