@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
    <h1>Home</h1>
@stop

@section('content')
    @if(isset($user))
    <div class="col-md-2">
        <ul class="list-group">
            <li class="list-group-item">Name:</li>
            <li class="list-group-item">Email:</li>
            <li class="list-group-item">Role:</li>
            <li class="list-group-item">Date created:</li>
        </ul>
    </div>
    <div class="col-md-3">
        <ul class="list-group">
            <li class="list-group-item">{{ $user->name }}</li>
            <li class="list-group-item">{{ $user->email }}</li>
            <li class="list-group-item">
                @role('admin') Admin @endrole
                @role('senior.moderator') Senior Moderator @endrole
                @role('middle.moderator') Middle Moderator @endrole
                @role('junior.moderator') Junior Moderator @endrole
            </li>
            <li class="list-group-item">{{ date_format($user->created_at,'d.m.Y')}}</li>
        </ul>
    </div>

    {{--<div class="col-md-2">
        <a href="{{ url(route('change password')) }}" class="btn btn-info" role="button">Change Password</a>
    </div>--}}
    <div class="clearfix"></div>

    {{--<div class="panel panel-default">
        <div class="panel-body">Permission</div>
        <div class="col-md-12">
            <ul class="list-group">
                <li class="list-group-item">Permission 1:</li>
                <li class="list-group-item">Permission 2:</li>
                <li class="list-group-item">Permission 3:</li>
            </ul>
        </div>
    </div>--}}
    @endif
@stop